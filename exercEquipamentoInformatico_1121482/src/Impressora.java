public class Impressora extends EquipamentoInformatico {

// LASER, JACTO_DE_TINTA, MATRIZ_DE_PONTOS
    
    private int tipo;
    
    public static final int LASER = 0;
    public static final int JACTO_DE_TINTA = 1;
    public static final int MATRIZ_DE_PONTOS = 2;

    private static boolean[] impressorasPromocao = {false, false, false};

    public Impressora(String mar, String mod, double pC, int t) {
        super(mar, mod, pC);
        tipo = t;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public static void setImpressorasPromocao(int tipo) {
        impressorasPromocao[tipo] = true;
    }

    public static void setImpressorasSemPromocao(int tipo) {
        impressorasPromocao[tipo] = false;
    }

    public boolean estaEmPromocao() {
        return impressorasPromocao[getTipo()];
    }

    public String toString() {
        return super.toString() + "\nTipo impressora: " + getTipo()
                + "\n" + (estaEmPromocao() ? "Esta " : "Nao esta ") + "em promocao.";
    }
}
