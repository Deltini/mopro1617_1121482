public class Computador extends EquipamentoInformatico {
    
    private String tipoProcessador;
    
    public Computador( String mar, String mod, double pC, String tipo) {
        super(mar, mod, pC);
        tipoProcessador = tipo;
    }
    public boolean estaEmPromocao() {
        return false;
    }
    public String toString() {
        return super.toString() +
                "\nTipo de processador: " + tipoProcessador +
                "\n" + (estaEmPromocao() ? "Esta ": "Nao esta ") + "em promocao.";
    }
}
