public class TesteEquipamentoInformatico {

    public static void main(String[] args) throws java.io.IOException {

        EquipamentoInformatico[] eq = new EquipamentoInformatico[10];
        eq[0] = new Computador("IBM", "Pentium", 250, "Pentium");
        eq[1] = new Impressora("HP", "DeskJet", 130, Impressora.LASER);
        eq[2] = new Impressora("CANON", "P500", 130, Impressora.MATRIZ_DE_PONTOS);
        eq[3] = new Modem("ASUS", "55", 10, Modem.EXTERNO);
        
        Impressora.setImpressorasPromocao(Impressora.MATRIZ_DE_PONTOS);
        Modem.setModemsPromocao(Modem.EXTERNO);
        
        for (int i = 0; i < eq.length; ++i) {
            if (eq[i] != null) {
                System.out.println(eq[i]);
                System.out.println("Preco venda ao publico: " + eq[i].calculaPrecoVenda());
            }
        }
    }
}
