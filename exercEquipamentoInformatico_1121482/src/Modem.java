public class Modem extends EquipamentoInformatico {

    private int localizacao; // INTERNO, EXTERNO

    public static final int INTERNO = 0;
    public static final int EXTERNO = 1;
    private static boolean[] modemsPromocao = {false, false};

    public Modem(String marca, String modelo, double pC, int localizacao) {
        super(marca, modelo, pC);
        this.localizacao = localizacao;
    }

    public int getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(int localizacao) {
        this.localizacao = localizacao;
    }

    public static void setModemsPromocao(int tipo) {
        modemsPromocao[tipo] = true;
    }

    public static void setModemsSemPromocao(int tipo) {
        modemsPromocao[tipo] = false;
    }

    public boolean estaEmPromocao() {
        return modemsPromocao[getLocalizacao()];
    }

    public String toString() {
        return super.toString() + "\nLocalizacao do modem: " + getLocalizacao()
                + "\n" + (estaEmPromocao() ? "Esta " : "Nao esta ") + "em promocao.";
    }

}
