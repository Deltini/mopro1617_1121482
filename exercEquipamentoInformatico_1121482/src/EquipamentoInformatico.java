public class EquipamentoInformatico {

    private static int numId = 0;
    private int referencia;
    private String marca;
    private String modelo;
    private double precoCusto;

    public EquipamentoInformatico() {
        referencia = numId++;
    }

    public EquipamentoInformatico(String mar, String mod, double pC) {
        this();
        marca = mar;
        modelo = mod;
        precoCusto = pC;
    }

    public boolean estaEmPromocao() {
        return false;
    }

    public double calculaPrecoVenda() {
        double margemLucro;

        if (estaEmPromocao()) {
            margemLucro = 0.05;
        } else {
            margemLucro = 0.2;
        }
        return precoCusto * (1 + margemLucro) * 1.23;
    }

    public String toString() {
        return "\nReferencia: " + referencia
                + "\nMarca: " + marca
                + "\tModelo: " + modelo
                + "\nPreco de custo: " + precoCusto;

    }
}
