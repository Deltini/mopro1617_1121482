
public class Data {

    /**
     *  O ano da data
     */
    private int ano;

    /**
     * O mês da data
     */
    private int mes;

    /**
     * O dia da data
     */
    private int dia;

    /**
     * O ano da data por defeito
     */
    private static final int ANO_DEFEITO = 1;

    /**
     * O mes da data por defeito
     */
    private static final int MES_DEFEITO = 1;

    /**
     * O dia da data por defeito
     */
    private static final int DIA_DEFEITO = 1;

    /**
     * Nomes dos dias da Semana
     */
    private static String[] nomeDiaDaSemana = {"Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"};

    /**
     * Número de dias de cada mês do ano
     */
    private static int[] diasPorMes = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    /**
     * Nomes dos meses do ano
     */
    private static String[] nomeMes = {"Inválido", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"};

    /**
     * Constrói a instância de Data recebendo o ano, mês e dia
     * @param ano
     * @param mes
     * @param dia
     */
    public Data(int ano, int mes, int dia){
        this.ano = ano;
        this.mes = mes;
        this.dia = dia;
    }

    /**
     * Constrói uma instância de Data com a data por omissão
     */
    public Data(){
        this.ano = ANO_DEFEITO;
        this.mes = MES_DEFEITO;
        this.dia = DIA_DEFEITO;
    }

    /**
     * Devolve o ano da data
     * @return
     */
    public int getAno(){
        return this.ano;
    }

    /**
     * Devolve o mês da data
     * @return
     */
    public int getMes(){
        return this.mes;
    }

    /**
     * Devolve o dia da data
     * @return
     */
    public int getDia(){
        return this.dia;
    }

    /**
     * Modifica o ano, mês e dia da data
     * @param ano
     * @param mes
     * @param dia
     */
    public void setData(int ano, int mes, int dia){
        this.ano = ano;
        this.mes = mes;
        this.dia = dia;
    }

    /**
     * Devolve a descrição visual: diaDaSemana, dia de mês de ano
     * @return
     */
    public String toString(){
        return diaDaSemana() + ", " + this.dia + " de " + nomeMes[this.mes] + " de " + this.ano;
    }

    /**
     * Devolve a data no formato: %04d/%02d/%02d
     * @return
     */
    public String toAnoMesDiaString(){
        return String.format("%04d/%02d/%02d", this.ano, this.mes, this.dia);
    }

    /**
     * Devolve o dia da semana da data
     * @return
     */
    public String diaDaSemana(){
        int totalDias = contaDias();
        totalDias = totalDias % 7;
        return nomeDiaDaSemana[totalDias];
    }

    /**
     * Devolve true se a data do objecto for maior que a data recebida por parametro
     * @param outraData
     * @return
     */
    public boolean isMaior(Data outraData){
        int totalDias = contaDias();
        int totalDias1 = outraData.contaDias();
        return totalDias > totalDias1;
    }

    /**
     * Devolve a diferença, em numero, de dias entre a data do objecto e outra data recebida por parametro
     * @param outraData
     * @return
     */
    public int diferenca(Data outraData){
        int totalDias = contaDias();
        int totalDias1 = outraData.contaDias();
        return Math.abs(totalDias - totalDias1);
    }

    /**
     * Devolve a diferença, em numero, de dias entre a data do objecto e outra data recebida por parametro com ano, mês e dia
     * @param ano
     * @param mes
     * @param dia
     * @return
     */
    public int diferenca(int ano, int mes, int dia){
        int totalDias = contaDias();
        Data outraData = new Data(ano, mes, dia);
        int totalDias1 = outraData.contaDias();
        return Math.abs(totalDias - totalDias1);
    }

    /**
     * Devolve true se o ano recebido por parametro for bisexto
     * @param ano
     * @return
     */
    public static boolean isAnoBisexto(int ano){
        return ano % 4 == 0 && ano % 100 != 0 || ano % 400 == 0;
    }

    /**
     * Devolve o número de dias desde o dia 1/1/1 até à data
     * @return
     */
    public int contaDias(){
        int totalDias = 0;
        for(int i = 1; i < this.ano; i++) totalDias += isAnoBisexto(i) ? 366 : 365;
        for(int i = 1; i < this.mes; i++) totalDias += diasPorMes[i];
        totalDias += (isAnoBisexto(this.ano) && this.mes > 2) ? 1 : 0;
        totalDias += this.dia;
        return totalDias;
    }
}
