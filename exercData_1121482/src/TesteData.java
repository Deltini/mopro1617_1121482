/**
 * Created by ruide on 27/03/2017.
 */
public class TesteData {

    public static void main( String args[]){
        Data data1 = new Data(2017, 03, 27);
        System.out.println(data1);
        Data data2 = new Data(1974, 04, 25);
        System.out.println(data2.toAnoMesDiaString());
        System.out.println(data1.isMaior(data2) ? "data1 mais recente que data2" : "data1 menos recente que data2");
        System.out.println("Diferença, em dias, entre data1 e data2: " + data1.diferenca(data2) + " dias.");
        System.out.println("Diferença, em dias, entre data1 e Natal: " + data1.diferenca(2017, 12, 25) + " dias.");
        System.out.println("Dia da semana data2: " + data2.diaDaSemana());
        //System.out.println("Ano Bisexto (data2)? " + (data2.isAnoBisexto() ? "Sim" : "Não")); Não é possivel chamar um método static a partir de uma instância
        System.out.println("Ano Bisexto (Data)? " + (Data.isAnoBisexto(data2.getAno()) ? "Sim" : "Não"));
    }
}
