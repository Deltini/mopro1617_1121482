/**
 * Created by Rui on 20/03/2017.
 */
public class Retangulo {
    /**
     * Lado A
     */
    private double a;

    /**
     * Lado B
     */
    private double b;

    /**
     * Construtor sem parâmetros
     */
    public Retangulo()
    {
        this.a = 0;
        this.b = 0;
    }

    /**
     * Construtor com parâmetros
     * @param a
     * @param b
     */
    public Retangulo( double a, double b )
    {
        this.a = a;
        this.b = b;
    }

    /**
     * Consulta lado A
     * @return
     */
    public double getA()
    {
        return this.a;
    }

    /**
     * Consulta lado B
     * @return
     */
    public double getB()
    {
        return this.b;
    }

    /**
     * Modificar lado A
     * @param a
     */
    public void setA( double a )
    {
        this.a = a;
    }

    /**
     * Modificar lado B
     * @param b
     */
    public void setB( double b )
    {
        this.b = b;
    }

    /**
     * Cálculo da área
     * @param a
     * @param b
     * @return
     */
    public double area()
    {
        return getA()*getB();
    }

    /**
     * Cálculo do perimetro
     * @param a
     * @param b
     * @return
     */
    public double perimetro()
    {
        return 2*(getA()+getB());
    }

    /**
     * Método String
     * @return
     */
    public String toString() {
        return "Área = " + area() + "\nPerimetro = " + perimetro();
    }
}
