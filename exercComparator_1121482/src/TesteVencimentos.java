import java.util.ArrayList;
import java.util.Collections;

public class TesteVencimentos {

    public static void main(String[] args){

        TrabalhadorPeca tp = new TrabalhadorPeca("Jorge Silva", 20, 30);
        TrabalhadorComissao tc = new TrabalhadorComissao("Susana Ferreira", 500.0f, 1500.0f, 6f);
        TrabalhadorHora th = new TrabalhadorHora("Carlos Miguel", 160, 3.5f);

        ArrayList<Trabalhador> trabalhadores  = new ArrayList<Trabalhador>();

        trabalhadores.add(tp);
        trabalhadores.add(tc);
        trabalhadores.add(th);

        //Comparable
        Collections.sort(trabalhadores);
        System.out.println("\n## Vencimentos ordenados por ordem crescente ##");
        for(int i = 0; i < trabalhadores.size(); i++)
                System.out.println(trabalhadores.get(i).calcularVencimento()+"€");

        //Comparable
        Collections.sort(trabalhadores, Collections.reverseOrder());
        System.out.println("\n## Vencimentos ordenados por ordem decrescente ##");
        for(int i = 0; i < trabalhadores.size(); i++)
            System.out.println(trabalhadores.get(i).calcularVencimento()+"€");

        //Comparator
        Collections.sort(trabalhadores, new OrdenarNome());
        System.out.println("\n## Nomes ordenados alfabeticamente ##");
        for (Trabalhador t : trabalhadores)
            System.out.println(t.getNome());
    }
}
