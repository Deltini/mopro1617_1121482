import java.util.Comparator;


public class OrdenarNome implements Comparator<Trabalhador> {
    public int compare(Trabalhador a, Trabalhador b)
    {
        return a.getNome().compareTo(b.getNome());
    }
}
