# README #

Repositório dos problemas resolvidos de MOPROG 16/17

### Estrutura ###

* exerc<Nome> - PL<Número_PL>:EX<Número_Exercicio>

### Ficheiros ###

* exercCumrimentar - PL1:EX2
* exercRetangulo - PL1:EX3
* exercData - PL2:EX1
* exercTempo - PL2:EX2
* exercFiguras - PL3:EX1
* exercVencimentos - PL3:EX2
* exercEquipamentoInformatico - PL4:EX1
* exercComparable - PL5:EX1
* exercComparator - PL5:EX2
