public class TesteTempo {

    public static void main(String[] args) {
        Tempo t1 = new Tempo(5, 30, 12);
        System.out.println(t1);
        t1.adicionarSegundo();
        System.out.println(t1);
        Tempo t2 = new Tempo(5, 31, 30);
        System.out.println(t2);
        System.out.println("t1 > t2? " + (t1.isMaior(t2) ? "Sim" : "Não"));
        System.out.println("t1 > t2? " + (t1.isMaior(23, 7, 4) ? "Sim" : "Não"));
        System.out.println("Diferença entre t1 e t2 em segundos: " + t1.diferencaSegundos(t2));
        System.out.println("Diferença entre t1 e t2 em Tempo: "  + t1.diferencaTempo(t2));
    }
}
