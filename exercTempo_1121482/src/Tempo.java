
public class Tempo {

    /**
     *  O hora da data
     */
    private int hora;

    /**
     * O minuto da data
     */
    private int minuto;

    /**
     * O segundo da data
     */
    private int segundo;

    /**
     * O hora da data por defeito
     */
    private static final int HORA_DEFEITO = 0;

    /**
     * O minuto da data por defeito
     */
    private static final int MINUTO_DEFEITO = 0;

    /**
     * O segundo da data por defeito
     */
    private static final int SEGUNDO_DEFEITO = 0;

    /**
     * Constrói a instância de Data recebendo o hora, minuto e segundo
     * @param hora
     * @param minuto
     * @param segundo
     */
    public Tempo(int hora, int minuto, int segundo){
        if(minuto > 59)
            minuto = MINUTO_DEFEITO;
        if(segundo > 59)
            segundo = SEGUNDO_DEFEITO;

        this.hora = hora;
        this.minuto = minuto;
        this.segundo = segundo;
    }

    /**
     * Constrói uma instância de Data com a data por omissão
     */
    public Tempo(){
        this.hora = HORA_DEFEITO;
        this.minuto = MINUTO_DEFEITO;
        this.segundo = SEGUNDO_DEFEITO;
    }

    /**
     * Devolve o hora da data
     * @return
     */
    public int getHora(){
        return this.hora;
    }

    /**
     * Devolve o minuto da data
     * @return
     */
    public int getMinuto(){
        return this.minuto;
    }

    /**
     * Devolve o segundo da data
     * @return
     */
    public int getSegundo(){
        return this.segundo;
    }

    /**
     * Modifica o hora, minuto e segundo da data
     * @param hora
     * @param minuto
     * @param segundo
     */
    public void setTempo(int hora, int minuto, int segundo){
        this.hora = hora;
        this.minuto = minuto;
        this.segundo = segundo;
    }

    /**
     * Devolve a descrição visual: segundoDaSemana, segundo de minuto de hora
     * @return
     */
    public String toString(){
        return this.hora + ":" + this.minuto + ":" + this.segundo;
    }

    /**
     *
     * @return
     */
    public void adicionarSegundo(){
        this.segundo++;

        if(this.segundo > 59){
            this.segundo = 0;
            this.minuto++;
        }

        if(this.minuto > 59){
            this.minuto = 0;
            this.hora++;
        }
    }

    /**
     *
     * @param outroTempo
     * @return
     */
    public boolean isMaior(Tempo outroTempo){
        int totalSegundos = contaSegundos();
        int totalSegundos1 = outroTempo.contaSegundos();
        return totalSegundos > totalSegundos1;
    }

    /**
     *
     * @param hora
     * @param minuto
     * @param segundo
     * @return
     */
    public boolean isMaior(int hora, int minuto, int segundo){
        int totalSegundos = contaSegundos();
        Tempo outroTempo = new Tempo(hora, minuto, segundo);
        int totalSegundos1 = outroTempo.contaSegundos();
        return totalSegundos > totalSegundos1;
    }

    /**
     *
     * @param outroTempo
     * @return
     */
    public int diferencaSegundos(Tempo outroTempo){
        int totalSegundos = contaSegundos();
        int totalSegundos1 = outroTempo.contaSegundos();
        return Math.abs(totalSegundos - totalSegundos1);
    }

    public Tempo diferencaTempo(Tempo outroTempo){
        int totalSegundos = contaSegundos();
        int totalSegundos1 = outroTempo.contaSegundos();
        int total = Math.abs(totalSegundos - totalSegundos1);
        return new Tempo(total/3600, total/60%60, total%60);
    }

    /**
     *
     * @return
     */
    public int contaSegundos(){
        return this.hora*3600 + this.minuto*60 + this.segundo;
    }
}
