public class TesteVencimentos {

    public static void main(String[] args){

        TrabalhadorPeca tp = new TrabalhadorPeca("Jorge Silva", 20, 30);
        TrabalhadorComissao tc = new TrabalhadorComissao("Susana Ferreira", 500.0f, 1500.0f, 6f);
        TrabalhadorHora th = new TrabalhadorHora("Carlos Miguel", 160, 3.5f);

        Trabalhador[] trabalhadores = new Trabalhador[10];
        trabalhadores[0] = tp;
        trabalhadores[1] = tc;
        trabalhadores[2] = th;

        System.out.println("\n## Trabalhadores ##");
        for( int i = 0; i < trabalhadores.length; i++){
            if( trabalhadores[i] != null)
                System.out.println(trabalhadores[i]+"\n");

        }

        System.out.println("\n## Trabalhadores à Hora ##");
        for( int i = 0; i < trabalhadores.length; i++){
            if( trabalhadores[i] instanceof TrabalhadorHora)
                System.out.println(trabalhadores[i] + "\n");
        }

        System.out.println("\n## Vencimentos ##");
        for( int i = 0; i < trabalhadores.length; i++){
            if( trabalhadores[i] != null)
                System.out.println(trabalhadores[i].getNome() + " --- "
                + trabalhadores[i].calcularVencimento() + " €");
        }
    }
}
