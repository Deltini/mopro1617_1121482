/**
 * Created by Rui on 20/03/2017.
 */
public class Pessoa {

    /**
     * Nome da pessoa
     */
    private String nome;

    /**
     * Nome da pessoa por omissão
     */
    private static final String NOME_OMISSAO = "Desconhecido";

    /**
     * Idade da pessoa
     */
    private byte idade;

    /**
     * Construtor sem parâmetros
     */
    public Pessoa(){
        this.nome = NOME_OMISSAO;
        this.idade = 0;
    }

    /**
     * Construtor com parâmetro nome da pessoa
     * @param nome
     */
    public Pessoa( String nome ){
        this.nome = nome;
        this.idade = 0;
    }

    /**
     * Construtor com parâmetro nome e idade da pessoa
     * @param nome
     * @param idade
     */
    public Pessoa( String nome, byte idade ){
        this.nome = nome;
        this.idade = idade;
    }

    /**
     * Consultar o nome da pessoa
     * @return
     */
    public String getNome(){
        return this.nome;
    }

    /**
     * Modificar o nome da pessoa
     * @param nome
     */
    public void setNome( String nome ){
        this.nome = nome;
    }

    /**
     * Consultar a idade da pessoa
     * @return
     */
    public byte getIdade(){
        return this.idade;
    }

    /**
     * Modificar a idade da pessoa
     * @param idade
     */
    public void setIdade( byte idade ){
        this.idade = idade;
    }
    /**
     * Método toString
     * @return
     */
    public String toString(){
        // 2. return "O nome é " + this.nome;
        return this.nome + " tem " + this.idade + " anos.";
    }
}
